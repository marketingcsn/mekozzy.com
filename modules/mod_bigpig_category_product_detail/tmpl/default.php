<?php
	/**
	 * @package CATEGORY PRODUCT DETAIL
	 * @version 1.0.0
	 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * @copyright (c) 2020 Mekozzy Company. All Rights Reserved.
	 * @author BIGPIG
	 *
	 */
	
	defined('_JEXEC') or die;
	
	//	use Joomla\CMS\Factory;
	//	$document = Factory::getDocument();
	//	$document->addStyleSheet('modules/mod_bigpig_category_product_detail/assets/css/typography.css');
	//	$document->addStyleSheet('modules/mod_bigpig_category_product_detail/assets/css/style.css');
	//	$document->addStyleSheet('modules/mod_bigpig_category_product_detail/assets/css/responsive.css');
	//	$document->addScript(  'modules/mod_bigpig_category_product_detail/assets/js/bigpig.js');
	foreach ($categories as $i => $category) {
		$categories[$i]->childs = $categoryModel->getChildCategoryList($vendorId, $category->virtuemart_category_id);
		if ($level > 2) {
			foreach ($categories[$i]->childs as $j => $cat) {
				$categories[$i]->childs[$j]->childs = $categoryModel->getChildCategoryList($vendorId, $cat->virtuemart_category_id);
			}
		}
	}
	$currency = CurrencyDisplay::getInstance();
	
	ob_start();
?>
<div class="row">
    <div class="nav nav-tabs col-12" id="tablistproducbigpig" role="tablist">
        <a class="d-block nav-item active show" data-toggle="pill"
           href="#v-pills-all<?php echo $categories['0']->virtuemart_category_id ?>" role="tablist"
           aria-selected="true">Tất Cả</a>
		<?php
			foreach ($categories as $cate) {
				$db = JFactory::getDBO();
				$db->setQuery(' SELECT `virtuemart_product_id` FROM `#__virtuemart_product_categories` WHERE `virtuemart_category_id` =' . (int)$cate->virtuemart_category_id);
				$listIDProduct = $db->loadObjectList();
				?>
                <a class="nav nav-tabs text-center p-3">
                    <a class="d-block nav-item" data-toggle="pill"
                       href="#v-pills-<?php echo $cate->virtuemart_category_id ?>" role="tablist"
                       aria-controls="#v-pills-<?php echo $cate->virtuemart_category_id ?>"
                       aria-selected="false"><?php echo $cate->category_name ?></a>
                </a>
			
			<?php } ?>

    </div>
</div>
<div class="row">
    <div class="tab-content mt-0">
		<?php
			foreach ($categories as $cate) {
				$list[] += $cate->virtuemart_category_id;
			}
			$db = JFactory::getDBO();
			$db->setQuery(' SELECT `virtuemart_product_id` FROM `#__virtuemart_product_categories` WHERE `virtuemart_category_id`IN (' . implode(',', $list) . ')');
			$listIDProduct = $db->loadObjectList();
			if (isset($listIDProduct) && $listIDProduct) {
				foreach ($listIDProduct as $temp) {
					$listid[] += $temp->virtuemart_product_id;
				}
				?>
                <div class="tab-pane fade show active"
                     id="v-pills-all<?php echo $categories['0']->virtuemart_category_id ?>" role="tabpanel"
                     aria-labelledby="v-pills-all">
					<?php
						$db = JFactory::getDBO();
						$query = $db->getQuery(true);
						$query->select('*')
							->from("#__virtuemart_products_vi_vn as a")
							->where("a.virtuemart_product_id IN (" . implode(',', $listid) . ')');
						$db->setQuery($query);
						$data = $db->loadObjectList();
						
						if (!empty($data)) {
							for ($k = 1; $k <= 8; $k++) {
								
								if (!empty($data[$k])) {
									$dbb = JFactory::getDBO();
									$qs = "SELECT file_url FROM #__virtuemart_medias WHERE virtuemart_media_id = '" . $data[$k]->virtuemart_product_id . "'";
									$dbb->setQuery($qs);
									$results = $dbb->loadObjectList();
									
									$ddbb = JFactory::getDBO();
									$q = 'SELECT `product_price`,`price_quantity_start`,`price_quantity_end`,`product_tax_id`,`product_currency` FROM `#__virtuemart_product_prices` WHERE `virtuemart_product_id`="' . $data[$k]->virtuemart_product_id . '" ORDER BY `price_quantity_start` ';
									$db->setQuery($q);
									$multiprices = $db->loadObject();
									?>

                                    <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                            <a href="#">
                                                <img src="<?php echo is_file(JURI::root() . $results[0]->file_url) ? is_file(JURI::root() . $results[0]->file_url) : "images/noimages.png"; ?>"
                                                     
                                                     alt="<?php echo $data[$k]->product_name; ?>">
                                            </a>
                                            <h6 class="text-center"><?php echo $data[$k]->product_name; ?></h6>
                                            <p class="text-center"><?php echo $currency->createPriceDiv('salesPrice', '', $multiprices->product_price, FALSE, FALSE, 1.0, TRUE); ?></p>
                                    </div>
									
									<?php
								} else {
									?>
                                    <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                        <a href="#">
                                            <img src="images/noimages.png"
                                                 
                                                 alt="noimages">
                                        </a>
                                        <h6 class="text-center">No Name</h6>
                                        <p class="text-center"> No Price</p>
                                    </div>
									<?php
								}
							}
						}
					?>
                </div>
			
			<?php } else { ?>
                <div class="tab-pane fade" id="v-pills-all<?php echo $cate->virtuemart_category_id ?>"
                     role="tabpanel" aria-selected="true"
                     aria-labelledby="v-pills-all<?php echo $cate->virtuemart_category_id ?>">
                    <div class="col-12" role="tabpanel"><h6 class="text-center">Khong Co San Pham</h6></div>
                </div>
				<?php
			}
		?>
		
		<?php
			foreach ($categories as $cate) {
				$db = JFactory::getDBO();
				$query = $db->getQuery(true);
				$query->select('*')
					->from("#__virtuemart_product_categories as a")
					->where("a.virtuemart_category_id =" . $cate->virtuemart_category_id);
				$query->setLimit(8);
				$db->setQuery($query);
				
				$listIDProduct = $db->loadObjectList();
				
				if (is_array($listIDProduct) && count($listIDProduct) > 0) {
					?>
                    <div class="tab-pane fade" id="v-pills-<?php echo $cate->virtuemart_category_id ?>"
                         role="tabpanel" aria-labelledby="v-pills-<?php echo $cate->virtuemart_category_id ?>">
						<?php
							for ($i = 0; $i < 7; $i++) {
								if(!empty($listIDProduct[$i]->virtuemart_product_id))
								{
									$db = JFactory::getDBO();
									$query = $db->getQuery(true);
									$query->select('*')
										->from("#__virtuemart_products_vi_vn as a")
										->where("a.virtuemart_product_id =" . $listIDProduct[$i]->virtuemart_product_id);
									$db->setQuery($query);
									$data = $db->loadObject();
								}
								if (!empty($data->virtuemart_product_id)) {
									$dbb = JFactory::getDBO();
									$qs = "SELECT file_url FROM #__virtuemart_medias WHERE virtuemart_media_id = '" . $data->virtuemart_product_id . "'";
									$dbb->setQuery($qs);
									$res = $dbb->loadObjectList();
									
									$ddbb = JFactory::getDBO();
									$q = 'SELECT `product_price`,`price_quantity_start`,`price_quantity_end`,`product_tax_id`,`product_currency` FROM `#__virtuemart_product_prices` WHERE `virtuemart_product_id`="' . $data->virtuemart_product_id . '" ORDER BY `price_quantity_start` ';
									$db->setQuery($q);
									$prices = $db->loadObject();
									?>

                                    <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                        <a href="#">
                                            <img src="<?php echo is_file(JURI::root() . $results[0]->file_url) ? is_file(JURI::root() . $results[0]->file_url) : "images/noimages.png"; ?>"
                                                 
                                                 alt="<?php echo $data->product_name; ?>">
                                        </a>
                                        <h6 class="text-center"><?php echo $data->product_name; ?></h6>
                                        <p class="text-center"><?php echo $currency->createPriceDiv('salesPrice', '', $prices->product_price, FALSE, FALSE, 1.0, TRUE); ?></p>
                                    </div>
									
									<?php
								}
								else
								{
									?>
                                    <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                                            <a href="#">
                                                <img src="images/noimages.png"
                                                     
                                                     alt="noimages">
                                            </a>
                                            <h6 class="text-center">No Name</h6>
                                            <p class="text-center"> No Price</p>
                                    </div>
									<?php
								}
							}
						?>
                    </div>
					<?php
				}
				else
				{
					?>
                    <div class="tab-pane fade" id="v-pills-<?php echo $cate->virtuemart_category_id ?>"
                         role="tabpanel" aria-selected="false"
                         aria-labelledby="v-pills-<?php echo $cate->virtuemart_category_id ?>">
                        <div class="col-12" role="tabpanel"><h6 class="text-center">Khong Co San Pham</h6></div>
                    </div>
					<?php
				}
			}
		?>
    </div>

</div>
<div class="row xemthem">
    <a class="d-block nav-item loadmore"
		<?php
	       $caturl = JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id=' . $category->virtuemart_category_id);
       ?>
       href="<?php echo $caturl ?>"
       target="_blank">Xem Them </a>
</div>
<style>
    div#tablistproducbigpig {
        overflow: auto;
        white-space: nowrap;
        flex-wrap: nowrap;
    }

    /* .col-3 {
        display: grid;
        grid-template-columns: auto auto auto auto;
        grid-template-rows: 100px 300px;
        grid-gap: 10px;
    } */

    .xemthem {
        display: flex;
        flex-direction: column;
        -webkit-box-align: center;
        align-items: center;
    }
</style>
