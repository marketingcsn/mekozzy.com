<?php
	/**
	 * @package CATEGORY PRODUCT DETAIL
	 * @version 1.0.0
	 * @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
	 * @copyright (c) 2020 Mekozzy Company. All Rights Reserved.
	 * @author BIGPIG
	 *
	 */
	
	defined('_JEXEC') or die;
	if (!class_exists( 'VmConfig' )) require(JPATH_ROOT .'/administrator/components/com_virtuemart/helpers/config.php');
	
	VmConfig::loadConfig();
	
	$layout = $params->get('layout', 'default');
	
	require JModuleHelper::getLayoutPath($module->module, $layout);
?>


